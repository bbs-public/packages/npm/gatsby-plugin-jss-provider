"use strict";

var fs = require("fs");

var path = require("path");

var os = require("os"); // Write out a jss module to .cache.


exports.onPreBootstrap = function (_ref, pluginOptions) {
  var store = _ref.store;
  var program = store.getState().program;
  var module;

  if (pluginOptions.pathToConfigModule) {
    module = `module.exports = require("${path.join(program.directory, pluginOptions.pathToConfigModule)}")`;

    if (os.platform() == "win32") {
      module = module.split("\\").join("\\\\");
    }
  } else {
    module = "module.exports = function(jss){return {};};";
  }

  var dir = __dirname + "/.cache";

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(dir + "/jss.js", module);

  if (pluginOptions.minify) {
    const config = pluginOptions.minifyConfig ? JSON.stringify(pluginOptions.minifyConfig) : '';
    module = `const csso = require('csso');module.exports = function(str){return csso.minify(str, ${config}).css};`
  } else {
    module = "module.exports = function(str){return str;};";
  }
  fs.writeFileSync(dir + "/minify.js", module);
};
