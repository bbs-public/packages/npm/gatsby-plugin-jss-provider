import React from 'react'
import { jss, JssProvider, SheetsRegistry, ThemeProvider } from 'react-jss'
import configure from './.cache/jss.js'

export const wrapRootElement = ({ element, pathname }, { theme = {} }) => {
  const registry = new SheetsRegistry()
  const props = configure(jss)

  return (
    <JssProvider {...props} registry={registry}>
      <ThemeProvider theme={theme}>{element}</ThemeProvider>
    </JssProvider>
  )
};

// remove the JSS style tag generated on the server to avoid conflicts with the one added on the client
export const onInitialClientRender = () => {
  const ssStyles = window.document.getElementById(`server-side-jss`);
  ssStyles && ssStyles.parentNode.removeChild(ssStyles)
};
