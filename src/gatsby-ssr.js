import React from "react"
import { jss, JssProvider, SheetsRegistry, ThemeProvider } from "react-jss"
import configure from './.cache/jss.js'
import minify from './.cache/minify.js'

const sheetsRegistryManager = new Map()

export const wrapRootElement = ({ element, pathname }, { theme = {} }) => {
  const registry = new SheetsRegistry()
  const props = configure(jss)
  sheetsRegistryManager.set(pathname, registry)

  return (
    <JssProvider {...props} registry={registry}>
      <ThemeProvider theme={theme}>{element}</ThemeProvider>
    </JssProvider>
  )
};

export const onRenderBody = ({ setHeadComponents, pathname }) => {
  const sheets = sheetsRegistryManager.get(pathname)
  if (sheets) {
    setHeadComponents([
      <style
        type="text/css"
        id="server-side-jss"
        key="server-side-jss"
        dangerouslySetInnerHTML={{ __html: minify(sheets.toString()) }}
      />,
    ]);
    sheetsRegistryManager.delete(pathname)
  }
};
